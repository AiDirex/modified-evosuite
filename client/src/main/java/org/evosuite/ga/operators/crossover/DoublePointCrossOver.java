/*
 * Copyright (C) 2010-2018 Gordon Fraser, Andrea Arcuri and EvoSuite
 * contributors
 *
 * This file is part of EvoSuite.
 *
 * EvoSuite is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3.0 of the License, or
 * (at your option) any later version.
 *
 * EvoSuite is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with EvoSuite. If not, see <http://www.gnu.org/licenses/>.
 */
package org.evosuite.ga.operators.crossover;

import org.evosuite.ga.Chromosome;
import org.evosuite.ga.ConstructionFailedException;
import org.evosuite.utils.Randomness;

/**
 * Cross over individuals at two relative positions
 *
 * @author Aidin Rasti
 */
public class DoublePointCrossOver<T extends Chromosome<T>> extends CrossOverFunction<T> {

	private static final long serialVersionUID = 4841658184798156L;

	private SinglePointRelativeCrossOver<T> singleCrossOver;

	public DoublePointCrossOver(){
	  super();
	  this.singleCrossOver = new SinglePointRelativeCrossOver<>();
  }

	@Override
	public void crossOver(T parent1, T parent2)
	        throws ConstructionFailedException {
		if (parent1.size() <= 4 || parent2.size() <= 4) {
			this.singleCrossOver.crossOver(parent1, parent2);
			return;
		}

		T t1 = parent1.clone();
		T t2 = parent2.clone();
		// Choose a position in the middle
		float splitPoint1 = Randomness.nextFloat();
		float splitPoint2 = Randomness.nextFloat();

		int firstPos1 = ((int) Math.floor((t1.size() - 1) * splitPoint1));
		if(firstPos1 == 0){
      firstPos1++;
    }
    if(firstPos1 == (t1.size() - 1) || firstPos1 == (t1.size() - 2)){
      firstPos1--;
    }
		int firstPos2 = ((int) Math.floor((t2.size() - 1) * splitPoint1));

    int secondPos1 = ((int) Math.floor((t1.size() - 1) * splitPoint2)) + 1;
    int secondPos2 = ((int) Math.floor((t2.size() - 1) * splitPoint2)) + 1;

//    logger.error("firstPos1: " + firstPos1+"  firstPos2: " + firstPos2+"  secondPos1: "+secondPos1+"  secondPos2: " +secondPos2);
    while(secondPos1 <= (firstPos1 + 1) ){
      splitPoint2 = Randomness.nextFloat();
      secondPos1 = ((int) Math.floor((t1.size() - 1) * splitPoint2)) + 1;
      secondPos2 = ((int) Math.floor((t2.size() - 1) * splitPoint2)) + 1;
    }

//    logger.error("firstPos1: " + firstPos1+"  firstPos2: " + firstPos2+"  secondPos1: "+secondPos1+"  secondPos2: " +secondPos2);
//		logger.error("pos1: " + (secondPos2 - firstPos2 + firstPos1)+"  pos2: " + (secondPos1 - firstPos1 + firstPos2));
//		logger.error("sizet1: " + t1.size() +"  sizet2: " + t2.size() );
		parent1.crossOver(t2, firstPos1, firstPos2);
		parent2.crossOver(t1, firstPos2, firstPos1);


    parent1.crossOver(t1, secondPos2 - firstPos2 + firstPos1, secondPos1);
    parent2.crossOver(t2, secondPos1 - firstPos1 + firstPos2, secondPos2);
	}

}
